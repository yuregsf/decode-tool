object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = 'Decode Tool'
  ClientHeight = 385
  ClientWidth = 385
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object tabCesar: TPageControl
    Left = 0
    Top = 0
    Width = 385
    Height = 385
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'C'#233'sar'
      ExplicitWidth = 372
      object Label1: TLabel
        Left = 11
        Top = 62
        Width = 28
        Height = 13
        Caption = 'Texto'
      end
      object Label2: TLabel
        Left = 11
        Top = 108
        Width = 31
        Height = 13
        Caption = 'Chave'
      end
      object Label3: TLabel
        Left = 11
        Top = 164
        Width = 53
        Height = 13
        Caption = 'Resultados'
      end
      object btnBrute: TButton
        Left = 282
        Top = 79
        Width = 87
        Height = 25
        Caption = 'Brute Force'
        TabOrder = 0
        OnClick = btnBruteClick
      end
      object btnCript: TButton
        Left = 291
        Top = 125
        Width = 78
        Height = 25
        Caption = 'Cifrar'
        TabOrder = 1
        OnClick = btnCriptClick
      end
      object btnDec: TButton
        Left = 207
        Top = 125
        Width = 78
        Height = 25
        Caption = 'Decifrar'
        TabOrder = 2
        OnClick = btnDecClick
      end
      object edChave: TEdit
        Left = 11
        Top = 127
        Width = 80
        Height = 21
        NumbersOnly = True
        TabOrder = 3
        OnClick = edChaveClick
      end
      object edTexto: TEdit
        Left = 11
        Top = 81
        Width = 249
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 4
      end
      object lsbResultado: TListBox
        Left = 11
        Top = 183
        Width = 358
        Height = 155
        ItemHeight = 13
        TabOrder = 5
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 377
        Height = 41
        Align = alTop
        Caption = 'Cifra de C'#233'sar'
        Color = clWindow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Candara'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 6
        ExplicitLeft = 2
        ExplicitTop = 15
        ExplicitWidth = 373
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Base64'
      ImageIndex = 1
      ExplicitLeft = 20
      ExplicitTop = 28
      object Label4: TLabel
        Left = 3
        Top = 181
        Width = 28
        Height = 13
        Caption = 'Texto'
      end
      object Label5: TLabel
        Left = 3
        Top = 45
        Width = 35
        Height = 13
        Caption = 'Base64'
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 377
        Height = 41
        Align = alTop
        Caption = 'Base64'
        Color = clWindow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Candara'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        ExplicitTop = 8
      end
      object btnBase64Enc: TButton
        Left = 107
        Top = 169
        Width = 75
        Height = 25
        Caption = '^'
        TabOrder = 1
        OnClick = btnBase64EncClick
      end
      object mmTexto: TMemo
        Left = 3
        Top = 200
        Width = 371
        Height = 97
        TabOrder = 2
      end
      object mmBase64: TMemo
        Left = 3
        Top = 64
        Width = 371
        Height = 99
        TabOrder = 3
      end
      object btnBase64Dec: TButton
        Left = 188
        Top = 169
        Width = 75
        Height = 25
        Caption = 'v'
        TabOrder = 4
        OnClick = btnBase64DecClick
      end
    end
  end
end
