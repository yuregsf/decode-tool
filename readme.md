# Decode Tool

Decode Tool is a simple project made in Delphi to Encode and Decode texts.

## Download
Clicke [here](https://bitbucket.org/yuregsf/decode-tool/downloads/decodetool.exe) to download the executable.

## Author

**Yure Gabriel**

## Language
**Delphi**