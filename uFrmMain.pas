unit uFrmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ComCtrls,
  synacode;

type
  TFrmMain = class(TForm)
    edTexto: TEdit;
    btnBrute: TButton;
    btnDec: TButton;
    edChave: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Panel1: TPanel;
    lsbResultado: TListBox;
    btnCript: TButton;
    tabCesar: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    btnBase64Enc: TButton;
    mmTexto: TMemo;
    mmBase64: TMemo;
    Label4: TLabel;
    Label5: TLabel;
    btnBase64Dec: TButton;
    procedure btnBruteClick(Sender: TObject);
    procedure btnCriptClick(Sender: TObject);
    procedure btnDecClick(Sender: TObject);
    procedure edChaveClick(Sender: TObject);
    procedure btnBase64EncClick(Sender: TObject);
    procedure btnBase64DecClick(Sender: TObject);
  private
    { Private declarations }
    function AsciiToInt(Caracter: Char): Integer;
    function Criptografa(texto: string; chave: Integer): String;
    function DeCriptografa(texto: string; chave: Integer): String;
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.dfm}

function TFrmMain.AsciiToInt(Caracter: Char): Integer;
var
  I: Integer;
begin
  I := 32;
  while I < 255 do
  begin
    if Chr(I) = Caracter then
      Break;
    I := I + 1;
  end;
  Result := I;

end;

procedure TFrmMain.btnBase64DecClick(Sender: TObject);
begin
  mmTexto.Lines.Clear;
  mmTexto.Lines.Add(String(DecodeBase64(AnsiString(mmBase64.Lines.GetText))));
end;

procedure TFrmMain.btnBase64EncClick(Sender: TObject);
begin
  mmBase64.Lines.Clear;
  mmBase64.Lines.Add(String(EncodeBase64(AnsiString(mmTexto.Lines.GetText))));
end;

procedure TFrmMain.btnBruteClick(Sender: TObject);
var
  texto: string;
  J: Integer;
begin
  lsbResultado.Items.Clear;
  texto := edTexto.Text;
  for J := 1 to 25 do
  begin
    lsbResultado.Items.Add(Criptografa(texto, J));
  end;
end;

procedure TFrmMain.btnDecClick(Sender: TObject);
var
  texto: string;
  chave: Integer;
begin
  lsbResultado.Items.Clear;
  texto := edTexto.Text;
  chave := StrToInt(edChave.Text);
  lsbResultado.Items.Add(DeCriptografa(texto, chave));
end;

procedure TFrmMain.btnCriptClick(Sender: TObject);
var
  texto: string;
  chave: Integer;
begin
  lsbResultado.Items.Clear;
  texto := edTexto.Text;
  chave := StrToInt(edChave.Text);
  lsbResultado.Items.Add(Criptografa(texto, chave));
end;

function TFrmMain.Criptografa(texto: string; chave: Integer): String;
var
  I, totalm, menosm: Integer;
begin
  for I := 1 to length(texto) do
  begin
    if texto[I] = ' ' then
      Continue;
    texto[I] := Chr(AsciiToInt(texto[I]) + chave);
    if not((AsciiToInt(texto[I]) > 64) and (AsciiToInt(texto[I]) < 91)) then
    begin
      if ((AsciiToInt(texto[I])) > 90) then
      begin
        while (AsciiToInt(texto[I])) > 90 do
        begin
          totalm := AsciiToInt(texto[I]);
          menosm := totalm - 90;
          texto[I] := Chr(64 + menosm);
        end;
      end
    end

  end;
  Result := texto;
end;

function TFrmMain.DeCriptografa(texto: string; chave: Integer): String;
var
  I, totalm, menosm: Integer;
begin
  for I := 1 to length(texto) do
  begin
    if texto[I] = ' ' then
      Continue;
    texto[I] := Chr(AsciiToInt(texto[I]) - chave);
    if not((AsciiToInt(texto[I]) > 64) and (AsciiToInt(texto[I]) < 91)) then
    begin
      if ((AsciiToInt(texto[I])) < 65) then
      begin
        while (AsciiToInt(texto[I])) < 65 do
        begin
          totalm := AsciiToInt(texto[I]);
          menosm := 65 - totalm;
          texto[I] := Chr(91 - menosm);
        end;
      end
    end
  end;
  Result := texto;
end;

procedure TFrmMain.edChaveClick(Sender: TObject);
begin
  edChave.SelectAll;
end;

end.
